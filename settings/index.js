import { CLIENT_ID, CLIENT_SECRET } from "../resources/PRIVATE";
function HelloWorld(props) {
    return (
      <Page>
        <Section
          title={<Text bold align="center">Score Keeper Settings</Text>}>
          <Oauth
            settingsKey="oauth"
            title="OAuth Login"
            label="Google Login"
            status="Login"
            authorizeUrl="https://accounts.google.com/o/oauth2/v2/auth"
            requestTokenUrl="https://oauth2.googleapis.com/token"
            clientId={CLIENT_ID}
            clientSecret={CLIENT_SECRET}
            scope="openid"
            onAccessToken={async (data) => {
              console.log(data);
            }}
          />
          <TextInput
            label="Player Names (separate with commas)"
            placeholder="name1,name2,..."
            settingsKey="playerNames"
            type="name"
          />
        </Section>
      </Page>
    );
  }
  
  registerSettingsPage(HelloWorld);