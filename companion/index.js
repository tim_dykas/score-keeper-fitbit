import { inbox } from "file-transfer";
import { outbox } from "file-transfer";
import { encode } from "cbor";
import { settingsStorage } from "settings";
import { localStorage } from "local-storage";
import { me as companion } from "companion";
import { FIRESTORE_URL, OAUTH_URL, REFRESH_URL } from "../resources/PRIVATE";

const REFRESH_TOKEN = "refresh_token"
const LOCAL_ID = "local_id"
const ID_TOKEN = "id_token"
const REFRESHTOKEN = "refreshToken"
const LOCALID = "localId"
const IDTOKEN = "idToken"

//setting keys
const KEY_OAUTH = 'oauth'
const KEY_PLAYER_NAMES = 'playerNames';

/* FETCH FUNCTIONS *****************************************************************/

function fetchSignIn(newValue) {
  let payload = `{"${ID_TOKEN}":"${JSON.parse(newValue)[ID_TOKEN]}"}`;
  console.log(payload);

  let body = {requestUri:"https://app-settings.fitbitdevelopercontent.com/simple-redirect.html", postBody:ID_TOKEN+"="+JSON.parse(newValue)[ID_TOKEN]+'&providerId=google.com', returnSecureToken:true, returnIdpCredential:true};
  fetch(OAUTH_URL, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(res => res.text())
    .then(response => {
      console.log('Sign In Response:', response);
      let responseObject = JSON.parse(response);
      localStorage.setItem(REFRESH_TOKEN, responseObject[REFRESHTOKEN]);
      localStorage.setItem(ID_TOKEN, responseObject[IDTOKEN]);
      localStorage.setItem(LOCAL_ID, responseObject[LOCALID]);

      sendFile("NewId","NewId");

      processAllFiles();
      inbox.addEventListener("newfile", processAllFiles);
    }).catch(error => console.error('Sign In Error:', error));

  
}

function fetchRefreshToken(){
  let body = {grant_type:REFRESH_TOKEN, refresh_token:localStorage.getItem(REFRESH_TOKEN)};
  fetch(REFRESH_URL, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(res => res.text())
    .then(response => {
      console.log('Refresh Response:', response);
      let responseObject = JSON.parse(response);
      localStorage.setItem(REFRESH_TOKEN, responseObject[REFRESH_TOKEN]);
      localStorage.setItem(ID_TOKEN, responseObject[ID_TOKEN]);

      processAllFiles();
      inbox.addEventListener("newfile", processAllFiles);
    }).catch(error => console.error('Refresh Error:', error));

}

function postData(payload, filename){
  let id = localStorage.getItem(LOCAL_ID);
  if (id == null) {
    return;
  }
  let payloadObj = {};
  payloadObj[JSON.parse(payload).id] = JSON.parse(payload)
  console.log("Post Data ", payload);
    fetch(FIRESTORE_URL+id+'/games.json?auth=' + localStorage.getItem(ID_TOKEN), {
    method: 'PATCH',
    body: JSON.stringify(payloadObj),
    headers:{
      'Content-Type': 'application/json'
    }
  }).then(res => res.text())
  .then(response => {console.log('POST Response:', response)

  let string = response.status + "";

  outbox.enqueue(filename, encode(string));
  })
  .catch(error => console.error('Error:', error));
  
}

if(localStorage.getItem(REFRESH_TOKEN) != undefined){
  fetchRefreshToken();
}

/* FILE TRANFER WITH APP *********************************************************************/

// Process the inbox queue for files, and read their contents as text
async function processAllFiles() {
  let file;
  while ((file = await inbox.pop())) {
    const payload = await file.text();
    const filename = file.name;
    console.log(`file contents: ${payload}`);
    console.log(`file name: ${file.name}`)
    postData(payload, filename);
  }
}

// Process new files as they are received
inbox.addEventListener("newfile", processAllFiles);

// Also process any files that arrived when the companion wasn’t running
processAllFiles();

function sendFile(filename, string){
  outbox.enqueue(filename, encode(string));
}

/* SETTINGS CHANGES **********************************************************************/

// Settings have been changed
settingsStorage.addEventListener("change", (evt) => {
  console.log("Settings Change: ", evt.key, evt.newValue);
  if (evt.key == KEY_OAUTH) {
    fetchSignIn(evt.newValue);
  }else if(evt.key == KEY_PLAYER_NAMES){
    console.log(JSON.parse(evt.newValue)["name"])
    sendFile(evt.key, JSON.parse(evt.newValue)["name"]);
  }
});

// Settings were changed while the companion was not running
if (companion.launchReasons.settingsChanged) {
  console.log(KEY_PLAYER_NAMES, settingsStorage.getItem(KEY_PLAYER_NAMES));
  sendFile(KEY_PLAYER_NAMES, JSON.parse(settingsStorage.getItem(KEY_PLAYER_NAMES)));
  
  fetchSignIn(settingsStorage.getItem("oauth"));
}