# Score Keeper (Fitbit)

This app keeps track of points won by each team/player and displays both the rally score (_points_) and side-out score (_score_). The current server is indicated by a read bar behind the score of that team or player.

![Scoring Screen](https://drive.google.com/uc?export=download&id=1D8ZlU03T2mjlHX_0ty4XGwbCyzAHmmad)

Available for the Fitbit Versa, Versa Lite, and Versa 2 [here]("https://gallery.fitbit.com/details/a48a6cf3-03ec-4313-be67-023c88646f90")

Options for player or team names can be added in the Fitbit phone app. Two names can then be selected on the watch. Their first initals will be used as the icons on the scoring screen to assist in assigning points to the correct player or team.

Users can also sign in to Google in the Fitbit phone app as authorization to upload data to Firebase Realtime Database.

![Settings](https://drive.google.com/uc?export=download&id=1X2I3F24BzSEAN2nROiiI8O5OghMubLJd)

![Choose Players Screen](https://drive.google.com/uc?export=download&id=1D8EQ9lIsS5AZUZD_viHJKKmcvUv7-8D9)

![New Game Screen](https://drive.google.com/uc?export=download&id=1D8YXNjVAAdgL6dKvSjsM4vL-RccI0b0i)

On the data screen, users can see the points for the current game as a string of o's and 1's, and can delete points one by one from the end to undo mistakes. The base 64 code can also be seen for the current game, as well as any previous games stored on the watch.

![Data Screen](https://drive.google.com/uc?export=download&id=1DAQd4ZWhpFNS6IgaXsYbS2C90HtVPTBt)

![Data Screen with previous games](https://drive.google.com/uc?export=download&id=1DFMtXP59OYZjeO5OzaNPY-HvogSaR5XL)