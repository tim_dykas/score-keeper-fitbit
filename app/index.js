import { me as appbit } from "appbit";
import document from "document";
import * as fs from "fs";

//persistent data
var currentGame = {"id":0};
const currentFilename = "currentGame.txt";
var previousGames = [];
const previousFilename = "previousGames.txt";
var players = [];
const playersFilename = "players.txt";

//app states
const STATE_NEW_GAME = 0;
const STATE_SCORING = 1;
const STATE_DATA = 2;
const STATE_SELECT_PLAYERS = 3;
var state = STATE_NEW_GAME;

//combo button icon options
const COMBO_NUMBERS = 0;
const COMBO_LETTERS = 1;
const COMBO_ARROWS = 2;

//select players
let playerIndex = 0;
let selecting = 0;
let firstSelection = -1;

//helper constants
const base64Code = [
  "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
  "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
  "0","1","2","3","4","5","6","7","8","9","+","/"
];

/* UI REFERENCES ********************************************************************************************************/

//svg containers
let comboButtons = document.getElementById("combo-buttons");
let nameGameDisplay = document.getElementById("new-game");
let scoringDisplay = document.getElementById("scoring");
let dataDisplay = document.getElementById("data-popup");
let changePlayersDisplay = document.getElementById("change-players");

//combo buttons
let btnTR = document.getElementById("btn-tr");
let btnBR = document.getElementById("btn-br");

//new game screen
let btnNewGame = document.getElementById("new-game-button");

//scoring screen
let serveIndicators = [document.getElementById("serveIndicator1"), document.getElementById("serveIndicator2")];
let scoreText = [document.getElementById("player1Score"), document.getElementById("player2Score")];
let pointsText = [document.getElementById("player1Points"), document.getElementById("player2Points")];
let btnShowData = document.getElementById("showDataButton");

//data screen
let deleteBtn = dataDisplay.getElementById("btnLeft");
let dismissBtn = dataDisplay.getElementById("btnRight");

//select players screen
let btnChangePlayers = document.getElementById("change-players-button");
let textboxChangePlayers = changePlayersDisplay.getElementById("player-list")
let selectPlayer = changePlayersDisplay.getElementById("select-player");
let btnContinue = nameGameDisplay.getElementById("continue-game-button");

/* FUNCTIONS ***********************************************************************************************************/

/* HELPER FUNCTIONS ********************************************************************************************************/

/**
 * Shows and hides SVG containers based on newState. Sets combo button icons.
 * @param {APP_STATE} newState 
 */
function setDisplay(newState){
  state = newState
  switch(state){
    case STATE_NEW_GAME:
      comboButtons.style.display = "inline";
      setComboButtonIcons(COMBO_LETTERS);
      nameGameDisplay.style.display = "inline";
      scoringDisplay.style.display = "none";
      dataDisplay.style.display = "none";
      changePlayersDisplay.style.display = "none";
      break;
    case STATE_SCORING:
      comboButtons.style.display = "inline";
      setComboButtonIcons(COMBO_LETTERS);
      nameGameDisplay.style.display = "none";
      scoringDisplay.style.display = "inline";
      dataDisplay.style.display = "none";
      changePlayersDisplay.style.display = "none";
      break;
    case STATE_DATA:
      comboButtons.style.display = "none";
      nameGameDisplay.style.display = "none";
      scoringDisplay.style.display = "none";
      dataDisplay.style.display = "inline";
      changePlayersDisplay.style.display = "none";
      break;
    case STATE_SELECT_PLAYERS:
      comboButtons.style.display = "inline";
      setComboButtonIcons(COMBO_ARROWS);
      nameGameDisplay.style.display = "none";
      scoringDisplay.style.display = "none";
      dataDisplay.style.display = "none";
      changePlayersDisplay.style.display = "inline";
      break;
  }
}

function toBase64(binary){
  let code = '';
  for(var i = 0; i < binary.length/6; i++){
    var substring = binary.substring(i*6, (i+1)*6);
    var value = 0;
    for(var b = 0; b < substring.length; b++){
      value += parseInt(substring.charAt(b)) * Math.pow(2, substring.length - 1 - b);
    }
    console.log('Value: '+value + ' Char: '+base64Code[value]);
    code += base64Code[value];
    //encode the length of the of the last substring as 1-6
    if(i >= binary.length/6 - 1){
      code = substring.length + code;
    }
  }

  return code;
}

// SAVING AND LOADING FUNCTIONS *********************************************************************************************

function loadCurrentGame(){
  if(fs.existsSync(currentFilename)){
    currentGame = JSON.parse(fs.readFileSync(currentFilename, "json"));
    if(currentGame.id == undefined){
      currentGame.id = 0;
    }

    if(currentGame.gameText.length > 0){
      console.log("Previous data exists: '"+currentGame.gameText+"'");
      state = STATE_SCORING;
      setDisplay(state);
      analyzePoints();
    }

    if(currentGame.firstPointTime == undefined){
      console.log("Time: " + new Date().getTime())
      // currentGame.firstPointTime = new Date().getTime();
    }
  } 
}

function loadPreviousGames(){
  console.log('Load previous games');
  if(fs.existsSync(previousFilename)){
    console.log('exists');
    previousGames = JSON.parse(fs.readFileSync(previousFilename, "json"));
    console.log(JSON.stringify(previousGames));
  }
}

function loadPlayers(){
  console.log('Load Players');
  if(fs.existsSync(playersFilename)){
    console.log('exists');
    players = JSON.parse(fs.readFileSync(playersFilename, "json"));
    console.log(JSON.stringify(players));
  }
}

function saveCurrentGame(){
  fs.writeFileSync(currentFilename, JSON.stringify(currentGame), "json");
}

function savePreviousGames(){
  fs.writeFileSync(previousFilename, JSON.stringify(previousGames), "json");
}

function savePlayers(){
  console.log('Save players: ' + JSON.stringify(fs.writeFileSync(playersFilename, JSON.stringify(players), "json")));
  console.log('Save players');
}

function saveGameData(){
  loadPreviousGames();
  currentGame.gameText = toBase64(currentGame.gameText);
  previousGames.push(currentGame);
  savePreviousGames();
}

/* NEW GAME SCREEN FUNCTIONS ******************************************************************************/

btnNewGame.onactivate = function(evt) {
  console.log("New Game");
  btnContinue.style.display = "inline";
  setDisplay(STATE_NEW_GAME);
}

/* SCORING SCREEN FUNCTIONS *******************************************************************************/

function init(server){
  currentGame.id++;
  currentGame.gameText = server.toString();
  saveCurrentGame(currentGame.gameText);
  // firstServe = server;
  analyzePoints();
}

function setFirstPointTime(){
  console.log("First Point Time is " + new Date().getTime());
  currentGame.firstPointTime = new Date().getTime();
}

function givePoint(player){
  if(currentGame.gameText.length == 1){
    setFirstPointTime()
  }
  currentGame.gameText+=player
  saveCurrentGame(currentGame.gameText);
  analyzePoints();
}

function analyzePoints(){
  let scores = [0,0]; //How many times each player won a rally (rally scoring)
  let points = [0,0]; //How many times the server won a rally (side-out scoring)

  //First server
  var lastPoint = currentGame.gameText.charAt(0);

  console.log("GameText: " + currentGame.gameText + ' Last point: '+lastPoint);
  for(var i = 1; i < currentGame.gameText.length; i++){
    let thisPoint = currentGame.gameText.charAt(i);
    points[thisPoint]++;
    if(thisPoint == lastPoint){
      scores[thisPoint]++
    }
    lastPoint = thisPoint;
  } 
  serveIndicators[lastPoint].style.visibility = "visible";
  serveIndicators[(parseInt(lastPoint)+1)%2].style.visibility = "hidden";
  for(let player = 0; player < 2; player++){
    scoreText[player].text = scores[player];
    pointsText[player].text = points[player];
  }
}

function updateGameTextDisplay(){
  let code = toBase64(currentGame.gameText);
  console.log(code);
  
  dataDisplay.getElementById("game-data").text = "Code: " + code+ "\nFirst Serve: " + currentGame.gameText.charAt(0) + "\nPoints: " + currentGame.gameText.substring(1);

  if(previousGames.length > 0){
    let previousGameText = '';
    for(let i = 0; i < previousGames.length; i++){
      previousGameText += previousGames[i].gameText + '\n\n';
    }

    document.getElementById("previous-game-data").text = previousGameText;
  } else{
    document.getElementById("previous-game-data").text = '';
  }

}

btnShowData.onactivate = function(evt) {
  loadPreviousGames();
  updateGameTextDisplay();
  for(let i = 0; i < previousGames.length; i++){
    if(previousGames[i].id == undefined){
      previousGames[i].id = -(i+1);
    }
    sendData(JSON.stringify(previousGames[i]),previousGames[i].id);
  }
  setDisplay(STATE_DATA);
}

/* DATA SCREEN FUNCTIONS **********************************************************************************/

deleteBtn.onclick = function(evt) {
  console.log("Delete");
  if(currentGame.gameText.length >= 2){
    currentGame.gameText = currentGame.gameText.substring(0, currentGame.gameText.length-1);
    updateGameTextDisplay();
  }
}

dismissBtn.onclick = function(evt) {
  console.log("Dismiss");
  analyzePoints();
  setDisplay(STATE_SCORING);
}

/* SELECT PLAYER SCREEN FUNCTIONS *************************************************************************/

btnChangePlayers.onactivate = function(evt) {
  console.log('Change players')
  loadPlayers();
  playerIndex = 0;
  setChangePlayerText();
  setDisplay(STATE_SELECT_PLAYERS);
}

selectPlayer.onactivate = function(evt){
  if(selecting == 0){
    firstSelection = playerIndex%players.length;
    playerIndex = 0;
    if(firstSelection == 0){
      playerIndex = 1;
    }
    selecting = 1;
    setChangePlayerText();
  }else{
    currentGame.player1 = players[firstSelection];
    currentGame.player2 = players[playerIndex%players.length];
    saveCurrentGame();
    firstSelection = -1;
    selecting = 0;
    setDisplay(STATE_SCORING);
  }
}

btnContinue.onclick = function(evt) {
  setDisplay(STATE_SCORING);
}

function setChangePlayerText(){
  let text = '';
  for(let i = playerIndex; i < players.length + playerIndex; i++){
    if(i%players.length != firstSelection){
      text += players[i%players.length]+'\n';
    }
  }
  console.log(JSON.stringify(players) + '\n' + text);
  textboxChangePlayers.text = text;
}

/* COMBO BUTTON FUNCTIONS *****************************************************************************/

btnTR.onactivate = function(evt) {
  onActivateComboButton(0);
}

btnBR.onactivate = function(evt) {
  onActivateComboButton(1);
}

function onActivateComboButton(zeroOrOne){
  console.log('Select Server');
  switch(state){
    case STATE_NEW_GAME:
      console.log(btnContinue.style.display);
      if(btnContinue.style.display == "inline"){
        console.log('inline');
        if(currentGame.gameText.length > 1){
          saveGameData();
        }
      }
      init(zeroOrOne);
      setDisplay(STATE_SCORING);
      break;
    case STATE_SCORING:
      givePoint(zeroOrOne)
      break;
    case STATE_SELECT_PLAYERS:
      playerIndex += (zeroOrOne == 1 ? 1 : -1);
      if(playerIndex == -1){
        playerIndex = players.length - 1;
      }
      if(playerIndex%players.length == firstSelection){
        playerIndex++;
      }
      setChangePlayerText();
      break;
  }
}

function setComboButtonIcons(option){
  switch(option){
    case COMBO_NUMBERS:
      btnTR.getElementById("combo-button-icon").image = "zero-outline.png";
      btnTR.getElementById("combo-button-icon-press").image = "zero.png";
      btnBR.getElementById("combo-button-icon").image = "one-outline.png";
      btnBR.getElementById("combo-button-icon-press").image = "one.png";
      break;
    case COMBO_LETTERS:
      let char1, char2;
      try{
        char1 = "letter_images/" + currentGame.player1.charAt(0).toUpperCase();
        char2 = "letter_images/" + currentGame.player2.charAt(0).toUpperCase();
      }catch(err){
        console.log('Player char error');
        char1 = 'zero';
        char2 = 'one'
      }
      btnTR.getElementById("combo-button-icon").image = char1+"-outline.png";
      btnTR.getElementById("combo-button-icon-press").image = char1+".png";
      btnBR.getElementById("combo-button-icon").image = char2+"-outline.png";
      btnBR.getElementById("combo-button-icon-press").image = char2+".png";
      break;
    case COMBO_ARROWS:
      btnTR.getElementById("combo-button-icon").image = "up-outline.png";
      btnTR.getElementById("combo-button-icon-press").image = "up.png";
      btnBR.getElementById("combo-button-icon").image = "down-outline.png";
      btnBR.getElementById("combo-button-icon-press").image = "down.png";
      break;
  }
}

/* BEGIN PROGRAM ***************************************************************************/
appbit.appTimeoutEnabled = false;
loadCurrentGame();

/* FILE TRANSFER ***************************************************************************/

import { listDirSync } from "fs";
import { inbox } from "file-transfer";
import { outbox } from "file-transfer";

// Event occurs when new file(s) are received
inbox.onnewfile = () => {
  console.log("New file!");
  let fileName;
  do {
    // If there is a file, move it from staging into the application folder
    fileName = inbox.nextFile();
    if (fileName) {
      if(fileName == "playerNames"){
        players = fs.readFileSync(fileName, "cbor").split(',');
        for(let player of players){
          player = player.trim();
        }
        savePlayers();
        console.log('New Names: ' + JSON.stringify(players))
      }else{//Uploading data responses
        let status = fs.readFileSync(fileName, "cbor");
        if(status == "200"){
          let id = fileName.substring(0, fileName.length - 4);
          console.log("Remove uploaded data: " + id);
          for(let i = 0; i < previousGames.length; i++){
            if(previousGames[i].id == id){
              previousGames.splice(i,1);
              break;
            }
          }
        }
      }     
    }
  } while (fileName);

  savePreviousGames();

  //Delete all files except for persistent save files
  const listDir = listDirSync("/private/data");
  let dirIter;
  while((dirIter = listDir.next()) && !dirIter.done) {
    if(dirIter.value != currentFilename && dirIter.value != previousFilename && dirIter.value != playersFilename){
      console.log("Delete File: " + dirIter.value);
      fs.unlinkSync(dirIter.value);
    }
  }
  
};

function sendData(data, filename){
  fs.writeFileSync(filename + ".txt", data, "ascii");
  outbox
    .enqueueFile("/private/data/" + filename + ".txt")
    .then((ft) => {
      console.log(`Transfer of ${ft.name} successfully queued.`);
    })
    .catch((error) => {
      console.log(`Failed to schedule transfer: ${error}`);
    })
}

